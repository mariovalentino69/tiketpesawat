<?php
/**
 * Description of airline_flight_model.
 * Used for getting airline flight data.
 * @author Mario Valentino.
 */
class Airline_flight_model extends CI_Model{
    
    /**
     * This is constructor.
     */
    public function __construct() {
        parent::__construct();
    }
    
    
    /**
     * This getFlightDataByAirline method.
     * Getting flight data based on carrier code.
     * @param string $carrierId.
     * @access public.
     */
    public function getFlightDataByAirline($carrierId){
        $this->db->select('fl.departure_date AS departure_date, 
                           fl.best_price AS price,
                           fair.airline_name AS maskapai,
                           fairport1.airport_name AS departure_airport,
                           fairport2.airport_name AS arrival_airport,
                           f_area1.area_name AS departure_area,
                           f_area2.area_name AS arrival_area'
                           );
        $this->db->from('f_airline_daily_best_price fl');
        $this->db->join('f_airline fair'        , 'fair.carrier_cd=fl.carrier_cd', 'inner');
        $this->db->join('f_airport fairport1'   , 'fairport1.airport_iata_cd = fl.departure_airport_cd', 'inner');
        $this->db->join('f_area_ref f_area1'    , 'f_area1.area_id = fairport1.airport_area_id', 'inner');
        $this->db->join('f_airport fairport2'   , 'fairport2.airport_iata_cd = fl.arrival_airport_cd', 'inner');
        $this->db->join('f_area_ref f_area2'    , 'f_area2.area_id = fairport2.airport_area_id', 'inner');
        $this->db->where('fl.carrier_cd '       , mysql_real_escape_string($carrierId));
        //$this->db->where('fl.departure_date >= ', mysql_real_escape_string(strftime('%Y-%m-%d')));
        //$this->db->where('fl.departure_date <=' , mysql_real_escape_string(strftime('%Y-%m-%d',strtotime('+7days'))));
        $this->db->order_by('fl.departure_date' , 'ASC');
        $this->db->limit(7);
        
        $query = $this->db->get();
        return $query->result();
    }
    
    
    /**
     * This getFlightDataByAirlineToArea method.
     * Getting flight data based on carrier code and area.
     * @param string $carrierCd.
     * @param string $areaName.
     * @access public.
     */
    public function getFlightDataByAirlineToArea($carrierCd, $areaName){
        $areaName = str_replace(" ", "%", $areaName);
        
        //echo $areaName;exit;
        
        $this->db->select('flight.departure_date AS tgl_keberangkatan, 
                           flight.best_price AS harga_tiket,
                           fair.airline_name AS maskapai,
                           farea1.area_name AS kota_keberangkatan,
                           farea2.area_name AS kota_tujuan',FALSE);
        $this->db->from('f_airline_daily_best_price flight');
        $this->db->join('f_airline fair'                , 'fair.carrier_cd = flight.carrier_cd','inner');
        $this->db->join('f_airport fairport1'           , 'fairport1.airport_iata_cd = flight.departure_airport_cd','inner');
        $this->db->join('f_airport fairport2'           , 'fairport2.airport_iata_cd = flight.arrival_airport_cd','inner');
        $this->db->join('f_area_ref farea1'             , 'farea1.area_id = fairport1.airport_area_id','inner');
        $this->db->join('f_area_ref farea2'             , 'farea2.area_id = fairport2.airport_area_id','inner');
        $this->db->where('flight.carrier_cd'            , $carrierCd);
        $this->db->where('farea2.area_name LIKE '       , '%'.$areaName.'%');
        //$this->db->where('flight.departure_date >= '    , mysql_real_escape_string(strftime('%Y-%m-%d')));
        //$this->db->where('flight.departure_date <='     , mysql_real_escape_string(strftime('%Y-%m-%d',strtotime('+7days'))));
        $this->db->order_by('flight.departure_date'     , 'ASC');
        $this->db->limit(7);
        
        $query = $this->db->get();
        return $query->result();
    }
    
    
    /**
     * This getFlightDataByAirlineAreaToArea method.
     * Getting flight data based on carrier code and area 1 and area 2.
     * @param string $carrierCd.
     * @param string $areaFrom.
     * @param string $areaTo.
     * @access public.
     */
    public function getFlightDataByAirlineFromAreaToArea($carrierCd, $areaFrom, $areaTo){
        $areaFrom = str_replace(" ", "%", $areaFrom);
        $areaTo   = str_replace(" ", "%", $areaTo);
        //echo $areaName;exit;
        
        $this->db->select('flight.departure_date AS tgl_keberangkatan, 
                           flight.best_price AS harga_tiket,
                           fair.airline_name AS maskapai,
                           farea1.area_name AS kota_keberangkatan,
                           farea2.area_name AS kota_tujuan',FALSE);
        $this->db->from('f_airline_daily_best_price flight');
        $this->db->join('f_airline fair'                , 'fair.carrier_cd = flight.carrier_cd','inner');
        $this->db->join('f_airport fairport1'           , 'fairport1.airport_iata_cd = flight.departure_airport_cd','inner');
        $this->db->join('f_airport fairport2'           , 'fairport2.airport_iata_cd = flight.arrival_airport_cd','inner');
        $this->db->join('f_area_ref farea1'             , 'farea1.area_id = fairport1.airport_area_id','inner');
        $this->db->join('f_area_ref farea2'             , 'farea2.area_id = fairport2.airport_area_id','inner');
        $this->db->where('flight.carrier_cd'            , $carrierCd);
        $this->db->where('farea1.area_name LIKE '       , '%'.$areaFrom.'%');
        $this->db->where('farea2.area_name LIKE '       , '%'.$areaTo.'%');
        //$this->db->where('flight.departure_date >= '    , mysql_real_escape_string(strftime('%Y-%m-%d')));
        //$this->db->where('flight.departure_date <='     , mysql_real_escape_string(strftime('%Y-%m-%d',strtotime('+7days'))));
        $this->db->order_by('flight.departure_date'     , 'ASC');
        $this->db->limit(7);
        
        $query = $this->db->get();
        return $query->result();
    }
    
    /**
     * This is getFlightDataByAirlineBasedOnDate method.
     * Getting flight data based on carrier code and date.
     * @param string $carrierCd.
     * @param string $date.
     * @access public.
     */
    public function getFlightDataByAirlineBasedOnDate($carrierCd, $date){
        $this->db->select('flight.departure_date AS tgl_keberangkatan, 
                           flight.best_price AS harga_tiket,
                           fair.airline_name AS maskapai,
                           farea1.area_name AS kota_keberangkatan,
                           farea2.area_name AS kota_tujuan',FALSE);
        $this->db->from('f_airline_daily_best_price flight');
        $this->db->join('f_airline fair'                , 'fair.carrier_cd = flight.carrier_cd','inner');
        $this->db->join('f_airport fairport1'           , 'fairport1.airport_iata_cd = flight.departure_airport_cd','inner');
        $this->db->join('f_airport fairport2'           , 'fairport2.airport_iata_cd = flight.arrival_airport_cd','inner');
        $this->db->join('f_area_ref farea1'             , 'farea1.area_id = fairport1.airport_area_id','inner');
        $this->db->join('f_area_ref farea2'             , 'farea2.area_id = fairport2.airport_area_id','inner');
        $this->db->where('flight.carrier_cd'            , $carrierCd);
        if($date['from'] != '' && $date['to'] != ''){
            $this->db->where('flight.departure_date >=' , mysql_real_escape_string(strftime('%Y-%m-%d',strtotime($date['from']))));
            $this->db->where('flight.departure_date <=' , mysql_real_escape_string(strftime('%Y-%m-%d', strtotime($date['to']))));
        }else{
            $this->db->where('flight.departure_date'    , mysql_real_escape_string(strftime('%Y-%m-%d', strtotime($date['from']))));
            $this->db->limit(5);
        }
        
        $query = $this->db->get();
        return $query->result();
    }
}
