<?php
/**
 * Description of airline_model.
 * This is airline model.
 * @author mario valentino.
 */
class Airline_model extends CI_Model {

    private $table = 'f_airline';
    /**
     * This is constructor
     */
    public function __construct() {
        parent::__construct();
    }
    
    /**
     * This is checkAirline method.
     * Used for whether airline is exist or not based on airline name.
     * @param string $airline.
     */
    public function checkAirline($airline){
        $query = $this->db->get_where($this->table, array('airline_name' => strtolower(mysql_real_escape_string($airline))));
        if($query->num_rows() > 0)
            return TRUE;
        else
            return FALSE;
    }
    
    
    /**
     * This is getAirlineIdByName method.
     * Getting airline id based on airline name.
     * @param string $airlineName.
     */
    public function getAirlineIdByName($airlineName){
        $this->db->select('carrier_cd');
        $this->db->from($this->table);
        $this->db->where('airline_name',  strtolower(mysql_real_escape_string($airlineName)));
        $query = $this->db->get();
        return $query->row()->carrier_cd;
    }
    
}
