<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of airline_rating_model.
 * This is Airline Rating model.
 * @author mario valentino.
 */
class Airline_rating_model extends CI_Model{
    private $table = 'f_airline_rating';
    /**
     * This is construtor
     */
    public function __construct() {
        parent::__construct();
    }
    
    /**
     * This is checkIpAddress method.
     * Checked whether ip address already exists in airline rating.
     * @param string $ipAddress.
     */
    public function checkIpAddress($ipAddress, $carrier_cd){
        $query = $this->db->get_where($this->table, array('ip_address' => mysql_real_escape_string($ipAddress),'carrier_cd' => mysql_real_escape_string($carrier_cd)));
        if($query->num_rows() > 0)
            return TRUE;
        else
            return FALSE;
    }
    
    /**
     * This is saveAirlineRating method.
     * Used for saving airline rating.
     * @param string $carrier_cd.
     * @param string $ipAddress.
     * @param integer $onTime.
     * @param integer $comfort.
     * @param integer $food.
     * @param integer $entertainment.
     * @param integer $moneyValue.
     */
    public function  saveAirlineRating($carrier_cd, $ipAddress, $onTime, $comfort, $food, $entertainment, $moneyValue){
        $data = array('carrier_cd'      => $carrier_cd,
                      'ip_address'      => $ipAddress,
                      'on_time'         => $onTime,
                      'comfortable'     => $comfort,
                      'food_beverages'  => $food,
                      'entertainment'   => $entertainment,
                      'money_value'     => $moneyValue);
        
        //print_r($data);exit;
        return $this->db->insert($this->table,$data);
    }
    
    
    /**
     * This is getVotingDataByAirline method.
     * Getting voting data based on airline name.
     * @param string $carrier_cd.
     */
    public function getVotingDataByAirline($carrier_cd){
        $this->db->select("ROUND(AVG(on_time),1) as avg_on_time,
                           ROUND(AVG(comfortable),1) as avg_comfort,
                           ROUND(AVG(food_beverages),1) as avg_food_beverages,
                           ROUND(AVG(entertainment),1) as avg_entertainment,
                           ROUND(AVG(money_value),1) as avg_money_value,
                           ROUND( (AVG(on_time) + 
                                   AVG(comfortable) + 
                                   AVG(food_beverages)+ 
                                   AVG(entertainment) + 
                                   AVG(money_value))/5
                               ,1) as total_average,
                            COUNT(carrier_cd) AS total_voter",FALSE);
        $this->db->from($this->table);
        $this->db->where('carrier_cd', mysql_real_escape_string($carrier_cd));
        $query = $this->db->get();
        return $query->row();
    }
    
}
