<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/*
| -------------------------------------------------------------------------
| URI ROUTING
| -------------------------------------------------------------------------
| This file lets you re-map URI requests to specific controller functions.
|
| Typically there is a one-to-one relationship between a URL string
| and its corresponding controller class/method. The segments in a
| URL normally follow this pattern:
|
|	example.com/class/method/id/
|
| In some instances, however, you may want to remap this relationship
| so that a different class/function is called than the one
| corresponding to the URL.
|
| Please see the user guide for complete details:
|
|	http://codeigniter.com/user_guide/general/routing.html
|
| -------------------------------------------------------------------------
| RESERVED ROUTES
| -------------------------------------------------------------------------
|
| There area two reserved routes:
|
|	$route['default_controller'] = 'welcome';
|
| This route indicates which controller class should be loaded if the
| URI contains no data. In the above example, the "welcome" class
| would be loaded.
|
|	$route['404_override'] = 'errors/page_missing';
|
| This route will tell the Router what URI segments to use if those provided
| in the URL cannot be matched to a valid route.
|
*/


// routing for airlines controllers. => http://pegipegi.com/tiket-pesawat/{airlines}/*
$route['dari\-([a-z\-]+)\-ke\-([a-z\-]+)']              = 'area/areaToarea/$1/$2/';

$route['([a-z\-]+)']                                    = 'airlines/index/$1/';
$route['([a-z\-]+)/perbandingan/([a-z\-]+)']            = 'airlines/comparison/$1/$2/';
$route['([a-z\-]+)/perbandingan']                       = 'airlines/comparison/$1/';
$route['([a-z\-]+)/promo']                              = 'airlines/promo/$1/';
$route['([a-z\-]+)/promo\-(([a-z]+)\-([0-9]{4}))']      = 'airlines/promo/$1/$2/';
$route['([a-z\-]+)/ke\-([a-z\-]+)']                     = 'airlines/toArea/$1/$2/';
$route['([a-z\-]+)/terminal']                           = 'airlines/terminal/$1/';
$route['([a-z\-]+)/(([0-9]{2})\-([a-z]+)\-([0-9]{4}))'] = 'airlines/basedOnDate/$1/$2/';
$route['([a-z\-]+)/(([a-z]+)\-([0-9]{4}))']             = 'airlines/basedOnDate/$1/$2/';
$route['([a-z\-]+)/(hari-ini)']                         = 'airlines/basedOnDate/$1/$2/';
$route['([a-z\-]+)/dari\-([a-z\-]+)\-ke\-([a-z\-]+)']   = 'airlines/areaToArea/$1/$2/$3/';
$route['([a-z\-]+)/office']                             = 'airlines/office/$1/';
$route['([a-z\-]+)/review']                             = 'airlines/review/$1/';
$route['([a-z\-]+)/review/(:any)']                      = 'airlines/reviewDetail/$1/$2/';
$route['([a-z\-]+)/rating']                             = 'airlines/rating/$1/';  







$route['default_controller'] = "welcome";
$route['404_override'] = '';


/* End of file routes.php */
/* Location: ./application/config/routes.php */