<section id="basedondatemonth-introduce">
    <div class="">
        <div><h2><?php echo $title;?></h2></div>
        <div>
            <table>
                <thead>
                    <tr>
                    <?php foreach($this->datemanipulation->days as $days):?>
                            <th><?php echo $days;?></th>
                    <?php endforeach;?>
                    </tr>
                </thead>
                
                <tbody>
                    <tr>
                    <?php if(count($dataFlight) >0): $weekDays = date('w',strtotime($parseData['from']));?>
                        <?php for($i = 0; $i < (count($dataFlight)+$weekDays); $i++):?>
                            <?php if(strlen(($i-$weekDays)+1)==1)$keys = str_pad(($i-$weekDays)+1,2,'0',STR_PAD_LEFT);else $keys= ($i-$weekDays)+1;?>
                            
                            
                            <?php if($i%7 == 0):?>
                                    <tr>
                            <?php endif;?>
                            
                            <?php if($i < $weekDays):?>
                                        <td></td>
                            <?php else: ?>
                                    <?php if($dataFlight[$keys]->tgl_keberangkatan >= strftime('%Y-%m-%d')):?>
                                    <td align="center" class="date_has_event">
                                        <span style="color:red"><?php echo ($i-$weekDays)+1;?></span><br>
                                        <span style="color:red"><?php echo $dataFlight[$keys]->maskapai != '' ? $dataFlight[$keys]->maskapai: '';?></span><br>
                                        <span style="color:red"><?php echo $dataFlight[$keys]->harga_tiket != '' ? 'IDR '.number_format($dataFlight[$keys]->harga_tiket,0,',','.'):'';?></span>
                                    </td>
                                    <?php else:?>
                                    <td>
                                        <span><?php echo ($i-$weekDays)+1;?></span><br>
                                    </td>
                                    <?php endif;?>
                            <?php endif;?>
                            
                            <?php if($i%7==6):?>
                                    </tr>
                            <?php endif;?>
                                    
                        <?php endfor;?>
                    <?php endif;?>
                    </tr>
                </tbody>
            </table>
        </div>
    </div>
</section>