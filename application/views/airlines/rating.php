<script type="text/javascript" src="<?php echo base_url().'assets/jquery/jquery-ui-1.9.2.custom/development-bundle/ui/jquery.ui.tabs.js';?>"></script>
<script type="text/javascript">
$(document).ready(function(){
    $(".data-tabs").tabs();
    $("li.tabs-promo a, li.tabs-compare a, li.tabs-review a").click(function() {
        location.href = $(this).attr('href');
    });
    
});
</script>
<section id="flight-introduce">
    <div class="flight-rating">
        <div class="rating-title">
            <h2><?php echo $title;?></h2>
            <span>Peringkat <?php echo $dataVoting->total_average != NULL ? $dataVoting->total_average : 0;?> /5 dari <?php echo $dataVoting->total_voter != NULL ? $dataVoting->total_voter : 0?> suara.</span>
        </div>
        
        <div class="rating-vote">
            <div>
                <div>Tepat waktu</div>
                <div><?php echo $dataVoting->avg_on_time != NULL ? $dataVoting->avg_on_time : 0;?></div>    
            </div>
            <div>
                <div>Tempat nyaman</div>
                <div><?php echo $dataVoting->avg_comfort != NULL ? $dataVoting->avg_comfort : 0;?></div> 
            </div>

            <div>
                <div>Makanan</div>
                <div><?php echo $dataVoting->avg_food_beverages != NULL ? $dataVoting->avg_food_beverages : 0;?></div> 
            </div>

            <div>
                <div>Hiburan</div>
                <div><?php echo $dataVoting->avg_entertainment != NULL ? $dataVoting->avg_entertainment : 0;?></div> 
            </div>

            <div>
                <div>Berharga bagi uang</div>
                <div><?php echo $dataVoting->avg_money_value != NULL ? $dataVoting->avg_money_value : 0;?></div> 
            </div>
            
            <div>
                <span>
                <?php 
                    if($validIp == TRUE){
                        if(isset($message)){
                            echo $message; 
                        }else{ 
                            echo "<a href='".base_url()."index.php/{$airline}.html'>Kembali</a>";
                        }
                    }else{
                        echo 'Terima kasih telah melakukan voting.';
                    }
                ?>
                </span>
            </div>
        </div>
    </div>
    
    <div class="flight-search">
        
    </div>
</section>

<section id="flight-data">
    <div class="data-tabs">
        <ul>
            <li><a href="#tabs-table">Ringkasan</a></li>
            <li class="tabs-promo"><a href="<?php echo base_url()."index.php/{$airline}/promo.html" ;?>">Promo</a></li>
            <li class="tabs-compare"><a href="<?php echo base_url()."index.php/{$airline}/perbandingan.html";?>">Perbandingan</a></li>
            <li class="tabs-review"><a href="<?php echo base_url()."index.php/{$airline}/review.html";?>">Ulasan</a></li>
        </ul>
    
        <div class="tabs-table">
            <div><h2>Tiket murah <?php echo $title;?> pada rute populer</h2></div>
            <div>
                <table>
                    <thead>
                        <tr>
                            <th>Tanggal Keberangkatan</th>
                            <th colspan="2">Harga Tiket</th>
                            <th>Maskapai</th>
                            <th>Bandara Keberangkatan</th>
                            <th>Bandara Kedatangan</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php
                        if(count($dataFlight) > 0):
                            foreach($dataFlight as $key => $object):
                                $date = $this->datemanipulation->dateToDateWord($object->departure_date);
                        ?>
                        <tr>
                            <td><?php echo $date;?></td>
                            <td>IDR </td>
                            <td align="right"><?php echo number_format($object->price,'0',',','.');?></td>
                            <td><?php echo $object->maskapai;?></td>
                            <td><?php echo $object->departure_area;?></td>
                            <td><a href="<?php echo base_url()."index.php/{$airline}/ke-".url_title($this->areaflight->parseAreaName($object->arrival_area),'-',TRUE).".html";?>"><?php echo $object->arrival_area;?></a></td>
                        </tr>
                        <?php 
                            endforeach;
                        else:
                        ?>
                        <tr>
                            <td colspan="5">Data tidak ditemukan.</td>
                        </tr>
                        <?php
                        endif;
                        ?>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</section>

