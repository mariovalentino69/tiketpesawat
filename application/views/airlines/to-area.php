<section id="area-introduce">
    <div class="area-tabular">
        <div class="tabular-title"><h2><?php echo $title;?></h2></div>
        <div>
            <table>
                <thead>
                    <tr>
                        <th>Tanggal Keberangkatan</th>
                        <th colspan="2">Harga Tiket</th>
                        <th>Maskapai</th>
                        <th>Bandara Keberangkatan</th>
                        <th>Bandara Kedatangan</th>
                    </tr>
                </thead>
                
                <tbody>
                <?php 
                if(count($dataFlight) > 0):
                    foreach($dataFlight as $key => $object):
                        $date = $this->datemanipulation->dateToDateWord($object->tgl_keberangkatan);
                ?>
                    <tr>
                        <td><?php echo $date;?></td>
                        <td>IDR </td>
                        <td align="right"><?php echo number_format($object->harga_tiket,0,',','.');?></td>
                        <td><?php echo $object->maskapai;?></td>
                        <td><?php echo $object->kota_keberangkatan;?></td>
                        <td><?php echo $object->kota_tujuan;?></td>
                    </tr>
                <?php 
                    endforeach;
                else: 
                ?>
                    <tr>
                        <td colspan="5">Data tidak ditemukan.</td>
                    </tr>
                <?php 
                endif;
                ?>
                </tbody>
            </table>
        </div>
    </div>
</section>

