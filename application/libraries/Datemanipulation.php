<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Datemanipulation.
 * Used for manipulating date time.
 * @author mario valentino.
 */
class Datemanipulation {
    protected $month = array('01' => 'Januari', 
                             '02' => 'Februari',
                             '03' => 'Maret',
                             '04' => 'April',
                             '05' => 'Mei',
                             '06' => 'Juni',
                             '07' => 'Juli',
                             '08' => 'Agustus',
                             '09' => 'September',
                             '10' => 'Oktober',
                             '11' => 'November',
                             '12' => 'Desember',
        );

    public $days = array(1 => 'Minggu', 2 => 'Senin', 3 => 'Selasa', 4 => 'Rabu', 5 => 'Kamis', 6 => 'Jumat', 7 => 'Sabtu');

    public function dateToDateWord($date){
        $parseDate  = strftime('%d', strtotime($date));
        $parseMonth = strftime('%m', strtotime($date));
        $parseYear  = strftime('%Y', strtotime($date));
        
        return $parseDate.'-'.$this->month[$parseMonth].'-'.$parseYear;
    }
    
    
    public function dateWordToDate($date){
        $newDateArray = array();
        if(strtolower($date) == 'hari ini'){
            $newDateArray = array('from' => strftime('%Y-%m-%d'), 'to' => '');
        } else {
            $explodeDate = explode(' ', $date);
            if(count($explodeDate) == 2){
                $dayFrom         = '01';
                $monthFrom       = array_search(ucwords($explodeDate[0]), $this->month);
                if($monthFrom == FALSE)return FALSE;
                $yearFrom        = $explodeDate[1];
                $dateFrom        = $dayFrom.'-'.$monthFrom.'-'.$yearFrom;
                
                $dayTo           = cal_days_in_month(CAL_GREGORIAN, $monthFrom, $yearFrom);
                $monthTo         = $monthFrom;
                $yearTo          = $yearFrom;
                $dateTo          = $dayTo.'-'.$monthTo.'-'.$yearTo;
                
                $newDateArray    = array('from' => strftime('%Y-%m-%d',strtotime($dateFrom)), 'to' => strftime('%Y-%m-%d',strtotime($dateTo)));
            }
            if(count($explodeDate) == 3){
                $dayFrom        = $explodeDate[0];
                $monthFrom      = array_search(ucwords($explodeDate[1]), $this->month);
                if($monthFrom == FALSE)return FALSE;
                $yearFrom       = $explodeDate[2];
                
                $dateFrom       = $dayFrom.'-'.$monthFrom.'-'.$yearFrom;
                $newDateArray   = array('from' => strftime('%Y-%m-%d',strtotime($dateFrom)), 'to' => '');
            }
        }
        return $newDateArray;
        
    }
    
    /**
     * This makeCalendarByDataFlight method.
     * Used for generating calendar using flight data.
     * @param array $dataFlight.
     */
    public function makeCalendarByDataFlight($dataFlight){
        if(count($dataFlight)){
            
        }
    }
}
