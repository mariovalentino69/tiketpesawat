<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Areaflight
 * This class used for parsing area name.
 * @author mario valentino.
 */
class Areaflight {
    
    /**
     * This is parseAreaName method.
     * Using for parsing area name.
     * @access public.
     */
    public function parseAreaName($areaName){
        $pattern        = array('/\)+/','/\(+/','/\,+/','/\.+/','/\-+/');
        $replacement    = '';
        
        $newAreaName = preg_replace($pattern, $replacement, $areaName);
        return $newAreaName;
    }
}
