<?php if(!defined('BASEPATH')) exit('No direct script access allowed');
/**
 * Description of airlines
 * This is airlines controller.
 * Used for display airline information based on airline name.
 * @author mario valentino.
 */
class Airlines extends CI_Controller{
    private $configuration;
    private $wordReserved = 'ke';
    
    /**
     * This is a constructor method.
     */
    public function __construct(){
        parent::__construct();
        $this->load->library(array('areaflight','datemanipulation'));
        $this->load->model(array('airline_model','airline_rating_model','airline_flight_model'));
    }
    
    
    /**
     * This is index method.
     * Display airline's main information based on {airline name}.
     * @param string $airlineName.
     * @access public.
     */
    public function index($airlineName){
        $airline        = $airlineName;
        //Replace {$airlineName} if parameter contains {-} sign.
        $airlineName    = $this->_replaceAirlineFormat($airlineName);
        
        //is airline name is exists in database.
        if(!$this->airline_model->checkAirline($airlineName)) {
            show_404();
        }
        
        //Getting carrier code based on airline name.
        $carrierCd  = $this->airline_model->getAirlineIdByName($airlineName);
        
        //Getting data flight based on carrier code 
        $dataFlight = $this->airline_flight_model->getFlightDataByAirline($carrierCd);
        
        //Getting voting data based on carrier code.
        $votingData = $this->airline_rating_model->getVotingDataByAirline($carrierCd);
        
        //check whether ip address is already save for voting.
        $checkIp = $this->airline_rating_model->checkIpAddress($this->input->ip_address(), $carrierCd);
        if($checkIp == 1) {
            $data['validIp'] = FALSE;
        } else {
            $data['validIp'] = TRUE;
        }
        
        
        $data['title']      = ucwords($airlineName);
        $data['airline']    = $airline; 
        $data['dataFlight'] = $dataFlight;
        $data['dataVoting'] = $votingData;
        
        $this->load->view('include/header', $data);
        $this->load->view('airlines/index', $data);
        $this->load->view('include/footer');
    }
    
    
    /**
     * This is rating method.
     * Used for display rating result.
     * @param string $airline.
     * @access public.
     */
    public function rating($airlineName){
        $airline        = $this->input->post('airline_name') != '' ? $this->input->post('airline_name') : $airlineName;
        $airlineFormat  = $this->_replaceAirlineFormat($airline);
        //getting airline id based on airline name.
        $carrierCd      = $this->airline_model->getAirlineIdByName($airlineFormat);
        $onTime         = $this->input->post('onTime');
        $comfort        = $this->input->post('comfort');
        $food           = $this->input->post('food');
        $entertainment  = $this->input->post('entertainment');
        $moneyValue     = $this->input->post('moneyValue');
        $ipAddress      = $this->input->ip_address();
        
        //is airline name is exists in database.
        if(! $this->airline_model->checkAirline($airlineFormat)) {
            show_404();
        }
        
        //echo $this->input->cookie('airlinevoter');
        $checkIp = $this->airline_rating_model->checkIpAddress($this->input->ip_address(), $carrierCd);
        //print_r($checkIp);
        if($checkIp == 1) {
            $data['validIp'] = FALSE;
        } else {
            $data['validIp'] = TRUE;
        }
        
        if($this->input->post('voterating')) {
            if($onTime == '' || $comfort == '' || $food == '' || $entertainment =='' || $moneyValue == ''){
                $data['message'] = 'Anda belum melakukan vote. <a href="'.base_url().'index.php/'.$airline.'.html">Silahkan coba lagi.</a>';
            }else{
                $cookie = array(
                    'name'   => 'airlinevoter',
                    'value'  => strftime("%d%m%Y%H%M%S"),
                    'expire' => time() + (365*24*60*60),
                    'domain' => '',
                    'path'   => '/',
                    'secure' => TRUE
                );

                $this->input->set_cookie($cookie); 

                if($this->airline_rating_model->saveAirlineRating($carrierCd, $ipAddress, $onTime, $comfort, $food, $entertainment, $moneyValue)){
                    $data['message'] = 'Terima kasih telah melakukan voting.';
                }
            }
        }
        //Getting data flight based on carrier code. 
        $dataFlight = $this->airline_flight_model->getFlightDataByAirline($carrierCd);
              
        //Getting voting data based on carrier code.
        $votingData = $this->airline_rating_model->getVotingDataByAirline($carrierCd);
        
        
        
        $data['title']      = ucwords($airlineName);
        $data['airline']    = $airline;
        
        $data['dataFlight'] = $dataFlight;
        $data['dataVoting'] = $votingData;
        
        
        $this->load->view('include/header', $data);
        $this->load->view('airlines/rating', $data);
        $this->load->view('include/footer');
    }
    
    
    /**
     * This is toArea method.
     * Display all flight information based on {airline name} and {area}.
     * @param string $airlineName.
     * @param string $toArea.
     * @access public.
     */
    public function toArea($airlineName, $toArea){
        //Replace {$airlineName} if parameter contains {-} sign.
        $airlineName    = $this->_replaceAirlineFormat($airlineName);
        
        //Replace {$toArea} if parameter contains {-} sign.
        $toArea = $this->_replaceAreaFormat($toArea);
        
        //is airline name is exists in database.
        if(! $this->airline_model->checkAirline($airlineName)) {
            show_404();
        }
        
        //getting airline id based on airline name.
        $carrierCd      = $this->airline_model->getAirlineIdByName($airlineName);
        
        //getting flight data based on carrier code and area.
        $dataFlight     = $this->airline_flight_model->getFlightDataByAirlineToArea($carrierCd, $toArea);
        //echo '<pre>';print_r($dataFlight);echo '</pre>';
        
        $data['title']          = "Tiket murah ".ucwords($airlineName)." ke ".ucwords($toArea);
        $data['dataFlight']     = $dataFlight;
        
        $this->load->view('include/header', $data);
        $this->load->view('airlines/to-area', $data);
        $this->load->view('include/footer');
    }
    
    
    /**
     * This is areaToArea method.
     * Display flight information based on {airline name} and {area 1} and  {area 2}
     * @param $airlineName string.
     * @param string $areaFrom.
     * @param string $areaToArea.
     * @access public.
     */
    public function areaToArea($airlineName, $areaFrom, $areaTo){
        //Replace {$airlineName} if parameter contains {-} sign.
        $airlineName    = $this->_replaceAirlineFormat($airlineName);
       
        //Replace {$areaFrom} if parameter contains {-} sign.
        $areaFrom = $this->_replaceAreaFormat($areaFrom);
        
        //Replace {$areaTo} if parameter contains {-} sign.
        $areaTo = $this->_replaceAreaFormat($areaTo);
        
        //is airline name is exists in database
        if(! $this->airline_model->checkAirline($airlineName)) {
            show_404();
        }
        
        //getting airline id based on airline name.
        $carrierCd      = $this->airline_model->getAirlineIdByName($airlineName);
        
        //getting flight data based on carrier code and area.
        $dataFlight     = $this->airline_flight_model->getFlightDataByAirlineFromAreaToArea($carrierCd, $areaFrom, $areaTo);
        
        
        
        $data['title']          = "Tiket murah ".ucwords($airlineName)." dari ".ucwords($areaFrom)." ke ".ucwords($areaTo);
        $data['dataFlight']     = $dataFlight;
        
        
        $this->load->view('include/header', $data);
        $this->load->view('airlines/area-to-area', $data);
        $this->load->view('include/footer');
    }
    
    
    /**
     * This is basedOnDate method.
     * Display flight information based on {airline name} and {specific date}
     * @param string $stringDate.
     * @access public.
     */
    public function basedOnDate($airlineName, $stringDate){
        //Replace {$airlineName} if parameter contains {-} sign.
        $airlineName    = $this->_replaceAirlineFormat($airlineName);
        
        //getting airline id based on airline name.
        $carrierCd      = $this->airline_model->getAirlineIdByName($airlineName);
        
        //is airline name is exists in array config
        if(! $this->airline_model->checkAirline($airlineName)) {
            show_404();
        }
        
        $extractDate = str_replace('-', ' ', $stringDate);
        
        $parseDate   = $this->datemanipulation->dateWordToDate($extractDate);
        
        if($parseDate == FALSE){
            show_404();
        }
        
        $dataFlight  = $this->airline_flight_model->getFlightDataByAirlineBasedOnDate($carrierCd, $parseDate);
        //print_r($dataFlight);
        $newDataFlight = array();
        $insertField = '';
        
        $totalDaysInMonth = strftime('%d',strtotime($parseDate['to']));
        if(count($dataFlight) > 0){
            if($parseDate['from'] !='' && $parseDate['to'] != ''){
                foreach($dataFlight as $key => $object){
                    $day    = strftime('%d', strtotime($object->tgl_keberangkatan));                       
                    if($object->tgl_keberangkatan == $insertField) {
                        unset($dataFlight[$key]);
                    }else{
                        $newDataFlight[$day] = $object;
                    }
                    $insertField = $object->tgl_keberangkatan;
                }
                
                for($i=1; $i <= $totalDaysInMonth;$i++){
                    if(strlen($i)==1)
                        $i = str_pad ($i, 2,'0',STR_PAD_LEFT);
                    
                    if(array_key_exists($i, $newDataFlight)){
                        $newDataFlight[$i] = $newDataFlight[$i];
                    }else{
                        $stdClass = new stdClass();
                        $stdClass->tgl_keberangkatan = '';
                        $stdClass->harga_tiket = '';
                        $stdClass->maskapai = '';
                        $stdClass->kota_keberangkatan = '';
                        $stdClass->kota_tujuan = '';
                        
                        $newDataFlight[$i] = $stdClass;
                    }
                }
                $data['parseData']  = $parseDate;
            }else{
                $newDataFlight = $dataFlight;
            }
        }else{
            if($parseDate['from'] !='' && $parseDate['to'] != ''){
                for($i = 1; $i <= $totalDaysInMonth; $i++){
                    if(strlen($i) == 1) $j = str_pad ($i, 2,'0',STR_PAD_LEFT);else $j = $i;
                    $stdClass = new stdClass();
                    $stdClass->tgl_keberangkatan = '';
                    $stdClass->harga_tiket = '';
                    $stdClass->maskapai = '';
                    $stdClass->kota_keberangkatan = '';
                    $stdClass->kota_tujuan = '';

                    $newDataFlight[$j] = $stdClass;
                }
                $data['parseData']  = $parseDate;
            }
        }
        
        //echo '<pre>';print_r($newDataFlight);echo '</pre>';
        $data['title']      = ucwords($airlineName)." tiket ".$extractDate;
        $data['dataFlight'] = $newDataFlight;
        
        
        $this->load->view('include/header', $data);
        if($parseDate['from'] != '' && $parseDate['to'] != ''){
            $this->load->view('airlines/based-on-date-month', $data);
        }else{
            $this->load->view('airlines/based-on-date', $data);
        }
        $this->load->view('include/footer');
    }
    
    
    /**
     * This is terminal method.
     * Display flight terminal based on {airline name}.
     * @param string $airlineName.
     * @access public.
     */
    public function terminal($airlineName){
        //Replace {$airlineName} if parameter contains {-} sign.
        $airlineName    = $this->_replaceAirlineFormat($airlineName);
        
        //is airline name is exists in database
        if(! $this->airline_model->checkAirline($airlineName)) {
            show_404();
        }
        
        $data['title']  = "{$airlineName}'s terminal.";
        
        $this->load->view('include/header', $data);
        $this->load->view('airlines/terminal', $data);
        $this->load->view('include/footer');
    }
    
    
    /**
     * This is comparison method.
     * Display comparison data based on {airline 1} and {airline 2}.
     * @param string $airlineName_1.
     * @param string $airlineName_2.
     * @access public.
     */
    public function comparison($airlineName_1, $airlineName_2 =''){
        //Replace {$airlineName_1} and {$airlineName_2} if parameter contains {-} with { }.
        $airlineName_1    = $this->_replaceAirlineFormat($airlineName_1);
        if($airlineName_2 !=''){
            $airlineName_2    = $this->_replaceAirlineFormat($airlineName_2);
            $data['title'] = "{$airlineName_1} vs {$airlineName_2}";
        }
        //is airline name is exists in database.
        if(! $this->airline_model->checkAirline($airlineName_1)) {
            show_404();
        }
        
        $data['title'] = "Perbandingan ". ucwords($airlineName_1);
        
        $this->load->view('include/header', $data);
        $this->load->view('airlines/comparison', $data);
        $this->load->view('include/footer');
    }
    
    
    /**
     * This is promo method.
     * Display all flight promo based on {airline name} and {month}-{year}.
     * @param string $airlineName.
     * @param string $datePromo.
     * @access public.
     */
    public function promo($airlineName, $datePromo=''){
        //Replace {$airlineName} if parameter contains {-} sign.
        $airlineName    = $this->_replaceAirlineFormat($airlineName);
        
        //is airline name is exists in database.
        if(! $this->airline_model->checkAirline($airlineName)) {
            show_404();
        }
        
        //Explode {$datePromo} become month and year format.
        if($datePromo !=''){
            $dateExplode    = explode('-', $datePromo);
            $month          = $dateExplode[0];
            $year           = $dateExplode[1];
            $data['title']  = "{$airlineName}'s promo on {$month} {$year}";
        }else{
            $data['title']  = "Promo ". ucwords($airlineName);
        }
        
        
        
        $this->load->view('include/header', $data);
        $this->load->view('airlines/promo', $data);
        $this->load->view('include/footer');
    }
    
    
    /**
     * This is office method.
     * Display flight office based on {airline name}.
     * @param string $airlineName.
     * @access public.
     */
    public function office($airlineName){
        //Replace {$airlineName} if parameter contains {-} sign.
        $airlineName    = $this->_replaceAirlineFormat($airlineName);
        
        //is airline name is exists in array config
        if(! $this->airline_model->checkAirline($airlineName)) {
            show_404();
        }
        
        $data['title']  = "{$airlineName}'s office.";
        
        $this->load->view('include/header', $data);
        $this->load->view('airlines/office', $data);
        $this->load->view('include/footer');
    }
    
    
    /**
     * This is review method.
     * Display flight office based on {airline name}.
     * @param string $airlineName.
     * @access public.
     */
    public function review($airlineName){
        //Replace {$airlineName} if parameter contains {-} sign.
        $airlineName    = $this->_replaceAirlineFormat($airlineName);
        
        //is airline name is exists in array config
        if(! $this->airline_model->checkAirline($airlineName)) {
            show_404();
        }
        
        $data['title']  = "{$airlineName}'s review.";
        
        $this->load->view('include/header', $data);
        $this->load->view('airlines/review', $data);
        $this->load->view('include/footer');
    }
    
    
    /**
     * This is review method.
     * Display flight office based on {airline name}.
     * @param string $airlineName.
     * @access public.
     */
    public function reviewDetail($airlineName, $title){
        //Replace {$airlineName} if parameter contains {-} sign.
        $airlineName    = $this->_replaceAirlineFormat($airlineName);
        
        //is airline name is exists in array config
        if(! $this->airline_model->checkAirline($airlineName)) {
            show_404();
        }
        
        $data['title']  = "{$airlineName}'s review detail about {$title}.";
        
        $this->load->view('include/header', $data);
        $this->load->view('airlines/review-detail', $data);
        $this->load->view('include/footer');
    }
    
    
    
    
    /**
     * This is _replaceAirlineFormat method.
     * Replace {-} with { } if {$airlineName} url contain {-}.
     * @param string $airlineName.
     * @access private.
     */
    private function _replaceAirlineFormat($airlineName){
        $airlineName    = str_replace("-", " ", $airlineName);
        return $airlineName;
    }
    
    
    /**
     * This is _replaceAreaFormat method.
     * Parsing {$toArea} parameter until get {$toArea} name.
     * @param string $toArea.
     * @access private.
     */
    private function _replaceAreaFormat($toArea){
        $toArea    = str_replace("-", " ", $toArea);
        return $toArea;
    }
}