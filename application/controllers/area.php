<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of area
 *
 * @author gala
 */
class Area extends CI_Controller {
    /**
     * This is areaToArea method.
     * Display flight information based on {airline name} and {area 1} and  {area 2}
     * @param $airlineName string.
     * @param string $areaFrom.
     * @param string $areaToArea.
     * @access public.
     */
    public function areaToArea($areaFrom, $areaTo){
        
        //Replace {$areaFrom} if parameter contains {-} sign.
        $areaFrom = $this->_replaceAreaFormat($areaFrom);
        
        //Replace {$areaTo} if parameter contains {-} sign.
        $areaTo = $this->_replaceAreaFormat($areaTo);
        
        //echo "{$airlineName} flight from {$areaFrom} to {$areaTo}<br>";
        echo "from {$areaFrom} to {$areaTo}";
        $data['title']  = "from {$areaFrom} to {$areaTo}";
        
        $this->load->view('include/header', $data);
        $this->load->view('airlines/area-to-area', $data);
        $this->load->view('include/footer');
    }
    
    /**
     * This is _replaceAreaFormat method.
     * Parsing {$toArea} parameter until get {$toArea} name.
     * @param string $toArea.
     * @access private.
     */
    private function _replaceAreaFormat($toArea){
        $toArea    = str_replace("-", " ", $toArea);
        return $toArea;
    }
}
